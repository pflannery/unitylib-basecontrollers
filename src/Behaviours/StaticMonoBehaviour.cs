﻿using UnityEngine;

namespace UnityLib.Behaviours {

  public abstract class StaticMonoBehaviour<Behaviour> : MonoBehaviour where Behaviour : MonoBehaviour {
    static Behaviour _instance;
    public static Behaviour Instance {
      get {
        if (_instance == null) {
          _instance = GameObject.FindObjectOfType<Behaviour>();
          if (_instance == null) {
            Debug.LogError("No controller instance found for " + typeof(Behaviour).FullName);
            return null;
          }
        }
        return _instance;
      }
    }
  }

}