﻿using UnityEngine;

namespace UnityLib.Models.Pooling {

  public class ObjectInstance
  {
    GameObject gameObject;
    Transform transform;

    bool hasPoolObjectComponent;
    PoolObject poolObjectScript;

    public ObjectInstance(GameObject objectInstance)
    {
      this.gameObject = objectInstance;
      this.gameObject.SetActive(false);
      this.transform = gameObject.transform;

      if (gameObject.GetComponent<PoolObject>())
      {
        this.hasPoolObjectComponent = true;
        this.poolObjectScript = gameObject.GetComponent<PoolObject>();
      }
    }

    public GameObject Instantiate(Vector3 position, Quaternion rotation)
    {
      if (hasPoolObjectComponent)
      {
        this.poolObjectScript.OnSpawned();
      }

      this.gameObject.SetActive(true);
      this.gameObject.transform.position = position;
      this.gameObject.transform.rotation = rotation;

      return this.gameObject;
    }

    public void SetName(string name)
    {
      this.gameObject.name = name;
    }

    public void SetParent(Transform parent)
    {
      this.gameObject.transform.SetParent(parent);
    }

  }

}