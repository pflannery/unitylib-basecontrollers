﻿using UnityEngine;

namespace UnityLib.Models.Pooling {

  public class PoolObject : MonoBehaviour {
    public virtual void OnSpawned() {

    }

    protected void Destroy() {
      gameObject.SetActive(false);
    }
  }

}