﻿using System.Collections.Generic;
using UnityEngine;
using UnityLib.Models.Pooling;
using UnityLib.Behaviours;

namespace UnityLib.Controllers.Pooling {

  public class GameObjectPoolController : StaticMonoBehaviour<GameObjectPoolController>
  {

    Dictionary<int, Queue<ObjectInstance>> poolDictionary = new Dictionary<int, Queue<ObjectInstance>>();
    Dictionary<string, GameObject> poolHolders = new Dictionary<string, GameObject>();

    public void CreatePool(GameObject prefab, int poolSize)
    {
      int poolKey = prefab.GetInstanceID();

      GameObject poolHolder = new GameObject(prefab.name + " pool");
      poolHolder.transform.SetParent(transform);

      if (poolDictionary.ContainsKey(poolKey) == false)
      {
        poolHolders.Add(prefab.name, poolHolder);
        Queue<ObjectInstance> newQueue = new Queue<ObjectInstance>();
        poolDictionary.Add(poolKey, newQueue);
        for (int i = 0; i < poolSize; i++)
        {
          ObjectInstance newObject = new ObjectInstance(Instantiate(prefab));
          newObject.SetParent(poolHolder.transform);
          newObject.SetName(prefab.name + " pool item " + i);
          newQueue.Enqueue(newObject);
        }
      }
    }

    public GameObject Instantiate(GameObject prefab, Vector3 position, Quaternion rotation)
    {
      int poolKey = prefab.GetInstanceID();
      if (poolDictionary.ContainsKey(poolKey))
      {
        Queue<ObjectInstance> queue = poolDictionary[poolKey];
        ObjectInstance objectToReuse = queue.Dequeue();
        queue.Enqueue(objectToReuse);
        return objectToReuse.Instantiate(position, rotation);
      }

      Debug.LogError("PoolManager.Instantiate: Invalid pool key");
      return null;
    }

    public GameObject GrowAndInstantiate(GameObject prefab, Vector3 position, Quaternion rotation)
    {
      int poolKey = prefab.GetInstanceID();

      if (poolDictionary.ContainsKey(poolKey) == false)
      {
        CreatePool(prefab, 1);
        return Instantiate(prefab, position, rotation);
      }
      else
      {
        Queue<ObjectInstance> queue = poolDictionary[poolKey];
        GameObject poolHolder = poolHolders[prefab.name];
        ObjectInstance newObject = new ObjectInstance(Instantiate(prefab));
        newObject.SetParent(poolHolder.transform);
        newObject.SetName(prefab.name + " pool item " + queue.Count);
        queue.Enqueue(newObject);
        return newObject.Instantiate(position, rotation);
      }

    }

  }

}