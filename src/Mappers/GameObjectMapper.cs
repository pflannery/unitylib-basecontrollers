﻿using System.Collections.Generic;
using UnityEngine;
using UnityLib.Behaviours;

namespace UnityLib.Mappers {

  public abstract class GameObjectMapper<Behaviour, EntityType> : StaticMonoBehaviour<Behaviour> where Behaviour : MonoBehaviour {

    protected static Dictionary<EntityType, GameObject> entityGameObjectMap = new Dictionary<EntityType, GameObject>();
    protected static Dictionary<GameObject, EntityType> gameObjectEntityMap = new Dictionary<GameObject, EntityType>();

    public static GameObject GetGameObject(EntityType entity) {
      return entityGameObjectMap[entity];
    }

    public static EntityType GetEntity(GameObject entity_go) {
      return gameObjectEntityMap[entity_go];
    }

    public static void Map(EntityType entity, GameObject entity_go) {
      entityGameObjectMap.Add(entity, entity_go);
      gameObjectEntityMap.Add(entity_go, entity);
    }

    public static void UnmapEntity(EntityType entity) {
      GameObject entity_go = entityGameObjectMap[entity];
      entityGameObjectMap.Remove(entity);
      gameObjectEntityMap.Remove(entity_go);
    }

    public static void UnmapGameObject(GameObject entity_go) {
      EntityType entity = gameObjectEntityMap[entity_go];
      entityGameObjectMap.Remove(entity);
      gameObjectEntityMap.Remove(entity_go);
    }

    public static ICollection<EntityType> Entities {
      get {
        return entityGameObjectMap.Keys;
      }
    }

  }

}